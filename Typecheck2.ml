[@@@warning "-27"] (* TODO *)
[@@@warning "-32"] (* TODO *)
[@@@warning "-30"]

module SMap = Map.Make(String)

module I = AST2.O

module O = struct
  type asttodo = [`TODO] (* occurrences of asttodo will point to some part of the original parser AST *)

  type name_and_region = {name: string; orig: Region.t}
  type type_name  = name_and_region
  type var_name   = name_and_region
  type field_name = name_and_region

  type pattern =
    PVar    of var_name
  | PWild
  | PInt    of Z.t
  | PBytes  of MBytes.t
  | PString of string
  | PUnit
  | PFalse
  | PTrue
  | PNone
  | PSome   of pattern
  | PCons   of pattern * pattern
  | PNull
  | PRecord of (field_name * pattern) SMap.t

  type type_constructor =
    Option
  | List
  | Set
  | Map

  type type_expr_case =
    Sum      of (type_name * type_expr) SMap.t
  | Record   of (field_name * type_expr) SMap.t
  | TypeApp  of type_constructor * (type_expr list)
  | Function of { arg: type_expr; ret: type_expr }
  | String
  | Bytes
  | Int
  | Unit
  | Bool

  and type_expr = { type_expr: type_expr_case; name: type_name option; orig: Region.t }

  type typed_var = { name:var_name; ty:type_expr; orig: asttodo }

  type type_decl = { name: type_name; ty:type_expr; orig: asttodo }

  type expr_case =
    App      of { operator: operator; arguments: expr list }
  | Var      of typed_var
  | Constant of constant
  | Record   of (field_name * expr) list
  | Lambda   of lambda

  and expr = { expr: expr_case; ty:type_expr; orig: asttodo }

  and decl = { var: typed_var; value: expr; orig: asttodo }

  and lambda = {
      parameter:    typed_var;
      declarations: decl list;
      instructions: instr list;
      result:       expr;
    }

  and operator_case =
    Function    of var_name
  | Constructor of var_name
  | UpdateField of field_name
  | GetField    of field_name
  | Or | And | Lt | Leq | Gt | Geq | Equal | Neq | Cat | Cons | Add | Sub | Mult | Div | Mod
  | Neg | Not
  | SetAdd
  | MapLookup

  and operator = { operator: operator_case; args:type_expr list; ret: type_expr; orig: asttodo }

  and constant =
    Unit
  | Int of Z.t | String of string | Bytes of MBytes.t
  | False | True
  | Null
  | EmptySet
  | CNone

  and instr =
    Assignment    of { name: var_name; value: expr; orig: asttodo }
  | While         of { condition: expr; body: instr list; orig: asttodo }
  | ForCollection of { list: expr; var: var_name; body: instr list; orig: asttodo }
  | Match         of { expr: expr; cases: (pattern * instr list) list; orig: asttodo }
  | ProcedureCall of { expr: expr; orig: asttodo } (* expr returns unit, drop the result. Similar to OCaml's ";". *)
  | Fail          of { expr: expr; orig: asttodo }

  type ast = {
      types           : type_decl list;
      storage_decl    : typed_var;
      declarations    : decl list;
      orig            : AST.t
    }
end

(* Utilities *)

let fold_map f a l =
  let f (acc, l) elem =
    let acc', elem' = f acc elem
    in acc', (elem' :: l) in
  let last_acc, last_l = List.fold_left f (a, []) l
  in last_acc, List.rev last_l

let map f l = List.rev (List.rev_map f l)

let mapi f l =
  let f (i, l) elem =
    (i + 1, (f i elem) :: l)
  in snd (List.fold_left f (0,[]) l)

let map_to_list m =
  List.rev (SMap.fold (fun field_name_string p l -> p :: l) m [])

(* From AST2.ml *)
let name_and_region_of_int i = O.{name = string_of_int i; orig = Region.ghost}
let named_list_to_map (l : (O.name_and_region * 'a) list) : (O.name_and_region * 'a) SMap.t =
  List.fold_left
    (fun m ((x,_) as p) ->
      let {name;_} : O.name_and_region = x in
      SMap.add name p m)
    SMap.empty
    l

(* Type and value environment manipulation *)

type te_ctors = O.type_expr SMap.t
type te_fields = O.type_expr SMap.t
type te = { types : O.type_expr list SMap.t; ctors : te_ctors; fields : te_fields }
type ve = O.type_expr list SMap.t
type tve = te * ve

let shadow_v (name : string) (typ : O.type_expr) (env : ve) : ve =
  SMap.update name (function None -> Some [typ] | Some tl -> Some (typ :: tl)) env

let shadow_t (name : string) (typ : O.type_expr) ({types=env; ctors; fields} : te) : te =
  let types = SMap.update name (function None -> Some [typ] | Some tl -> Some (typ :: tl)) env
  in {types;ctors;fields}

let lookup_v (name : string) (env : O.type_expr list SMap.t) : O.type_expr =
  match SMap.find_opt name env with
    Some (latest :: shadowed) -> latest
  | _                         -> failwith "Unbound variable"

let lookup_t (name : string) ({types=env; _} : te) : O.type_expr =
  match SMap.find_opt name env with
    Some (latest :: shadowed) -> latest
  | _                         -> failwith "Unbound type"

let lookup_ctor (name : string) ({ctors;_} : te) : O.type_expr =
  match SMap.find_opt name ctors with
    Some sum             -> sum
  | None                 -> failwith "Undeclared constructor"

let lookup_field (name : string) ({fields;_} : te) : O.type_expr =
  match SMap.find_opt name fields with
    Some record          -> record
  | None                 -> failwith "Undeclared field"

let string_of_name ({name;_} : I.name_and_region) = name

(* Annotation of AST nodes (OCaml declarations in reverse order
 * because declarations cannot use following ones) *)

let a_name_and_region ({name; orig} : I.name_and_region) : O.name_and_region =
  {name; orig}

let a_type_constructor (tve : tve) : I.type_constructor -> O.type_constructor = function
  Option -> Option
| List   -> List
| Set    -> Set
| Map    -> Map

let rec a_labeled_list_type_expr (tve : tve) (lt : (I.name_and_region * I.type_expr) SMap.t) (ctors : te_ctors) (fields : te_fields)
        : (O.name_and_region * O.type_expr) SMap.t * te_ctors * te_fields =
  let f (ctors, fields) (x,y) =
    let y, ctors, fields = a_type_expr tve y ctors fields in
    (ctors, fields), (a_name_and_region x, y) in
  let (ctors, fields), labeled =
    fold_map f (ctors,fields) (map snd (SMap.bindings lt)) in
  let labeled = List.fold_left (fun acc ((x : O.name_and_region),y) -> SMap.add x.name (x,y) acc)
                               SMap.empty labeled
  in labeled, ctors, fields

and a_type_app (tve : tve) (tc : I.type_constructor) (args : I.type_expr list) (ctors : te_ctors) (fields : te_fields)
    : O.type_expr_case * te_ctors * te_fields =
  let f (ctors, fields) arg =
    let arg, ctors, fields = a_type_expr tve arg ctors fields in
    (ctors,fields), arg in
  let (ctors, fields), args = fold_map f (ctors, fields) args in
  (TypeApp (a_type_constructor tve tc, args),
   ctors,
   fields)

and a_function_type_expr (tve : tve) (arg : I.type_expr) (ret : I.type_expr) (ctors : te_ctors) (fields : te_fields)
    : O.type_expr_case * te_ctors * te_fields =
  let arg, ctors, fields = a_type_expr tve arg ctors fields in
  let ret, ctors, fields = a_type_expr tve ret ctors fields in
  (Function {arg; ret},
   ctors,
   fields)

and a_sum tve lt ctors fields : (O.type_expr_case * te_ctors * te_fields) * string list * string list =
  let new_ctors = (map fst (SMap.bindings lt)) in
  let x, ctors, fields = a_labeled_list_type_expr tve lt ctors fields in
  (Sum x, ctors, fields), new_ctors, []

and a_record tve lt ctors fields : (O.type_expr_case * te_ctors * te_fields) * string list * string list =
  let new_fields = (map fst (SMap.bindings lt)) in
  let x, ctors, fields = a_labeled_list_type_expr tve lt ctors fields in
  (Sum x, ctors, fields), [], new_fields

and a_type_expr_case (tve : tve) (ctors : te_ctors) (fields : te_fields) : I.type_expr_case -> (O.type_expr_case * te_ctors * te_fields) * string list * string list = function
    Sum      lt         -> a_sum tve lt ctors fields
  | Record   lt         -> a_record tve lt ctors fields
  | TypeApp  (tc, args) -> a_type_app tve tc args ctors fields, [], []
  | Function {arg;ret}  -> a_function_type_expr tve arg ret ctors fields, [], []
  | String              -> (String, ctors, fields), [], []
  | Bytes               -> (Bytes, ctors, fields), [], []
  | Int                 -> (Int, ctors, fields), [], []
  | Unit                -> (Unit, ctors, fields), [], []
  | Bool                -> (Bool, ctors, fields), [], []

and a_type_expr (tve : tve) ({type_expr;name;orig} : I.type_expr) (ctors : te_ctors) (fields : te_fields) : O.type_expr * te_ctors * te_fields =
  let (type_expr, ctors, fields), new_ctors, new_fields =
    a_type_expr_case tve ctors fields type_expr in
  let name = match name with
      None -> None
    | Some name -> Some (a_name_and_region name) in
  let t : O.type_expr = {type_expr;name;orig} in
  let ctors = List.fold_left (fun ctors l -> SMap.add l t ctors)
                             ctors
                             new_ctors in
  let fields = List.fold_left (fun ctors l -> SMap.add l t ctors)
                             ctors
                             new_fields
  in t, ctors, fields

let a_type (te,ve : tve) ({name;ty;orig} : I.type_decl) : tve * O.type_decl =
  let {types;ctors;fields} : te = te in
  let ty,ctors,fields = a_type_expr (te,ve) ty ctors fields in
  let te = {types;ctors;fields} in
  let tve = shadow_t (string_of_name name) ty te, ve in
  let name = (a_name_and_region name) in
  tve, {name; ty; orig}

let a_types (tve : tve) (l : I.type_decl list) : tve * O.type_decl list =
  fold_map a_type tve l

let a_storage_decl ({types;ctors;fields},ve : tve) (storage_decl : I.typed_var) : tve * O.typed_var =
  let ({name;ty;orig} : I.typed_var) = storage_decl in
  let ty,ctors,fields = a_type_expr ({types;ctors;fields},ve) ty ctors fields in
  let ve = shadow_v (string_of_name name) ty ve in
  let name = a_name_and_region name in
  ({types;ctors;fields},ve), {name;ty;orig}

let rec labeled_equal (c1 : O.name_and_region * O.type_expr) (c2 : O.name_and_region * O.type_expr) =
  let ({name=name1;orig=_} : O.name_and_region), ty1 = c1 in
  let ({name=name2;orig=_} : O.name_and_region), ty2 = c2 in
  String.equal name1 name2 && type_expr_equal ty1 ty2

and labeled_list_equal l1 l2 =
  List.length l1 = List.length l2
  && List.for_all2 labeled_equal l1 l2

and type_constructor_equal (tc1 : O.type_constructor) (tc2 : O.type_constructor) =
  match tc1,tc2 with
| Option, Option -> true
| List,   List   -> true
| Set,    Set    -> true
| Map,    Map    -> true
| _              -> false

and type_app_equal (tc1, args1) (tc2, args2) =
  type_constructor_equal tc1 tc1
  && List.length args1 = List.length args2
  && List.for_all2 type_expr_equal args1 args2

and function_equal (arg1,ret1) (arg2,ret2) =
  type_expr_equal arg1 arg2
  && type_expr_equal ret1 ret2

and type_expr_case_equal (t1 : O.type_expr_case) (t2 : O.type_expr_case) : bool = match t1,t2 with
    (* TODO: write this with a single exhaustive pattern-matching with a 1-case + _ match in every clause *)
    Sum      m1,                  Sum      m2                  -> labeled_list_equal (map_to_list m1) (map_to_list m2)
  | Record   m1,                  Record   m2                  -> labeled_list_equal (map_to_list m1) (map_to_list m2)
  | TypeApp  (tc1, args1),        TypeApp  (tc2, args2)        -> type_app_equal (tc1, args1) (tc2, args2)
  | Function {arg=arg1;ret=ret1}, Function {arg=arg2;ret=ret2} -> function_equal (arg1,ret1) (arg2,ret2)
  | String,                       String                       -> true
  | Bytes,                        Bytes                        -> true
  | Int,                          Int                          -> true
  | Unit,                         Unit                         -> true
  | Bool,                         Bool                         -> true
  | _                                                          -> false

and type_expr_equal (t1 : O.type_expr) (t2 : O.type_expr) : bool =
  type_expr_case_equal t1.type_expr t2.type_expr

let check_type_expr_equal (expected : O.type_expr option) (actual : O.type_expr) : unit =
  match expected with
    None -> ()
  | Some expected ->
     if type_expr_equal expected actual then
       ()
     else
       (* TODO: trigger the message at the first difference, not for the overall type. *)
       failwith "got [actual] but expected [expected]"

let a_var_expr (te,ve : tve) (expected : O.type_expr option) (var_name : I.name_and_region) : O.expr =
  let actual = (lookup_v (string_of_name var_name) ve) in
  check_type_expr_equal expected actual;
  let expr_case : O.expr_case = Var { name = a_name_and_region var_name;
                                      ty   = actual;
                                      orig = `TODO } in
  { expr = expr_case; ty = actual; orig = `TODO }

and ctors_of_tve ({types=_;ctors;fields=_},_) = ctors
and fields_of_tve ({types=_;ctors=_;fields},_) = fields

let a_constant_expr (tve : tve) (expected : O.type_expr option) (constant : I.constant) : O.expr =
  let to_type_expr type_expr_case : O.type_expr =
    { type_expr = type_expr_case; name = None; orig = Region.ghost } in
  let actual : O.type_expr = match constant with
      Unit       -> to_type_expr Unit
    | Int      _ -> to_type_expr Int
    | String   _ -> to_type_expr String
    | Bytes    _ -> to_type_expr Bytes
    | False      -> to_type_expr Bool
    | True       -> to_type_expr Bool
    | Null     t -> let a, _todo_ignored_new_ctors, _new_fields =
                      a_type_expr tve t (ctors_of_tve tve) (fields_of_tve tve)
                    in a
    | EmptySet t -> let a, _todo_ignored_new_ctors, _new_fields =
                      a_type_expr tve t (ctors_of_tve tve) (fields_of_tve tve)
                    in a
    | CNone    t -> let a, _todo_ignored_new_ctors, _new_fields =
                      a_type_expr tve t (ctors_of_tve tve) (fields_of_tve tve)
                    in a
  in
  check_type_expr_equal expected actual;
  let c : O.constant = match constant with
      Unit       -> Unit
    | Int      i -> Int      i
    | String   s -> String   s
    | Bytes    b -> Bytes    b
    | False      -> False
    | True       -> True
    | Null     _ -> Null
    | EmptySet _ -> EmptySet
    | CNone    _ -> CNone in
  let expr_case : O.expr_case = Constant c in
  { expr = expr_case; ty = actual; orig = `TODO }

let rec a_field (tve : tve)
            (((expected_name : O.name_and_region), (expected    : O.type_expr option)),
             ((actual_name   : I.name_and_region), (actual_expr : I.expr)))
        : (O.name_and_region * O.expr) =
  let expected_name = expected_name.name in
  if String.equal expected_name (string_of_name actual_name) then
    (a_name_and_region actual_name),
    (a_expr tve expected actual_expr)
  else
    failwith "Expected field [expected] but got [actual]"

and a_record (tve : tve) (expected : O.type_expr option) (record : (I.field_name * I.expr) list)
    : O.expr =
  match expected with
    None ->
    let expected_and_field =
      map (fun ((actual_name, _) as actual) -> (a_name_and_region actual_name,None), actual) record in
    let expr_fields = (map (a_field tve) expected_and_field) in
    let expr_case : O.expr_case = Record expr_fields in
    let ty : O.type_expr_case =
      Record (map (fun (n, (e : O.expr)) -> n, e.ty) expr_fields
              |> named_list_to_map) in
    let ty : O.type_expr = {type_expr = ty; name = None; orig = Region.ghost} in
    ({ expr = expr_case; ty; orig = `TODO } : O.expr)
  | Some expected ->
    let {type_expr = expected_case; _} : O.type_expr = expected in
    let expected_fields = match expected_case with
        Record fields -> SMap.map (fun (n,t) -> (n,Some t)) fields
      | _ -> failwith "expected some_type but got record" in
    let expected_and_field =
      List.combine
        (map_to_list expected_fields)
        record (* TODO SHOULD BE (map_to_list record) *) in
    let expr_case : O.expr_case = Record (map (a_field tve) expected_and_field) in
    ({ expr = expr_case; ty = expected; orig = `TODO } : O.expr)


and f (operator : O.operator_case) (args : O.type_expr_case list) (ret : O.type_expr_case) : O.operator =
  let a_args arg : O.type_expr =
    { type_expr = arg; name = None; orig = Region.ghost } in
  let args =
    map a_args args
    (* |> mapi (fun i a -> name_and_region_of_int i, a) *)
    (* |> named_list_to_map *)
    (* |> (fun x -> (Record x : O.type_expr_case)) *)
    (* |> (fun x -> ({ type_expr = x; name = None; orig = Region.ghost } : O.type_expr)) *) in
  let ret : O.type_expr = { type_expr = ret; name = None; orig = Region.ghost } in
  (* let ty : O.type_expr = { type_expr = Function {arg=args;ret}; name = None; orig = Region.ghost } in *)
  {operator; args; ret; orig=`TODO}

and a_operator_function (te,ve) (expected_ret : O.type_expr option) (name : I.name_and_region) : O.operator =
  let function_type = lookup_v (string_of_name name) ve in
  match function_type with
    {type_expr = (Function {arg;ret}); _} ->
    check_type_expr_equal expected_ret ret;
    {
      operator = Function (a_name_and_region name);
      args=[arg];
      ret=ret;
      orig=`TODO
    }
  | _ -> failwith "Expected a function but got [actual]"

and a_operator_constructor (te,ve) (expected_ret : O.type_expr option) (name : I.name_and_region) : O.operator =
  match expected_ret with
    Some ({type_expr = Sum m;_} as expected_ret) ->
    (let ctor = SMap.find_opt (string_of_name name) m in
     match ctor with
       None ->
       failwith ("Expected the variant [expected] but got"
                 ^ " the constructor [name] which is not present in that variant.")
     | Some (_name, expected_arg_type) ->
        {operator = Constructor (a_name_and_region name);
         args     = [expected_arg_type];
         ret      = expected_ret;
         orig     = `TODO})
  | Some _ ->
     failwith "Expected [expected] but got a constructor"
  | None ->
     (match lookup_ctor (string_of_name name) te with
        {type_expr = Sum m;_} as expected_ret ->
        (let ctor = SMap.find_opt (string_of_name name) m in
         match ctor with
           None -> assert false
         | Some (_name, expected_arg_type) ->
            {operator = Constructor (a_name_and_region name);
             args     = [expected_arg_type];
             ret      = expected_ret;
             orig     = `TODO})
      | _ -> assert false)

and a_operator_x_field (te,ve) (expected_ret : O.type_expr option) (name : I.name_and_region) (operator : O.name_and_region -> O.operator_case) : O.operator =
  (match lookup_field (string_of_name name) te with
     {type_expr = Record m;_} as expected_record_type ->
     (let field_type = SMap.find_opt (string_of_name name) m in
      match field_type with
        None -> assert false
      | Some (_name, actual_ret) ->
         check_type_expr_equal expected_ret actual_ret;
         {operator = operator (a_name_and_region name);
          args     = [expected_record_type];
          ret      = actual_ret;
          orig     = `TODO})
   | _ -> assert false)

and a_operator tve (expected_ret : O.type_expr option) : I.operator -> O.operator = function
    Function    name ->
    a_operator_function tve expected_ret name
  | Constructor name ->
    a_operator_constructor tve expected_ret name
  | UpdateField name ->
     a_operator_x_field tve expected_ret name (fun n -> UpdateField n)
  | GetField name ->
     a_operator_x_field tve expected_ret name (fun n -> GetField n)
  | Or        -> f Or        [Bool;Bool] Bool
  | And       -> f And       [Bool;Bool] Bool
  | Lt        -> f Lt        [Int; Int] Bool
  | Leq       -> f Leq       [Int; Int] Bool
  | Gt        -> f Gt        [Int; Int] Bool
  | Geq       -> f Geq       [Int; Int] Bool
  | Equal     -> assert false (* Equal is handled directly by App *)
  | Neq       -> assert false (* Neq is handled directly by App *)
  | Cat       -> f Cat       [String; String] Bool
  | Cons      -> assert false (* Conses are handled directly by App *)
  | Add       -> f Add       [Int; Int] Int
  | Sub       -> f Sub       [Int; Int] Int
  | Mult      -> f Mult      [Int; Int] Int
  | Div       -> f Div       [Int; Int] Int
  | Mod       -> f Mod       [Int; Int] Int
  | Neg       -> f Neg       [Int; Int] Int
  | Not       -> f Not       [Bool]     Bool
  | Set       -> assert false (* Set  literals are handled directly by App *)
  | List      -> assert false (* List literals are handled directly by App *)
  | MapLookup -> assert false (* MapLookup is handled directly by App *)

(* TODO: the type implies that operators are functions, but the
   "arguments" imply that they take multiple arguments. *)
and a_app_expr_case (tve : tve) (expected_ret : O.type_expr option) (operator : I.operator) (arguments : I.expr list) : O.expr =
  let operator : O.operator = a_operator tve expected_ret operator in
  let expected : O.type_expr list = operator.args in
  let arguments : O.expr list = map (fun (e,a) -> a_expr tve (Some e) a) (List.combine expected arguments) in
  let expr_case : O.expr_case = App {operator;arguments}
  in { expr = expr_case; ty = operator.ret; orig = `TODO }

and a_collection_literal_expr_case_expected (type_constructor : O.type_constructor)
                                            (operator : O.operator_case)
                                            (empty : O.constant)
                                            (name : string)
                                            (tve : tve)
                                            (expected : O.type_expr)
    : I.expr list -> O.expr =
  function
    [] -> { expr = Constant empty; ty = expected; orig = `TODO }
  | hd :: tl ->
     (match expected with
        {type_expr = (TypeApp (type_constructor, [elem_type])); _} as list_type ->
        let expr_case : O.expr_case =
          App { operator = {operator;
                            args = [elem_type; list_type];
                            ret = list_type;
                            orig=`TODO};
                arguments = [a_expr tve (Some elem_type) hd;
                             a_collection_literal_expr_case_expected type_constructor operator empty name tve expected tl] }
        in { expr = expr_case; ty = expected; orig = `TODO }
      | _ -> failwith ("expected [expected] but got a " ^ name))

and a_collection_literal_expr_case (type_constructor : O.type_constructor)
                                   (operator : O.operator_case)
                                   (empty : O.constant)
                                   (name : string)
                                   (tve : tve)
                                   (expected : O.type_expr option)
    : I.expr list -> O.expr =
  function
    [] -> assert false          (* TODO: nsepseq *)
  | hd :: tl ->
     (match expected with
        Some expected ->
        a_collection_literal_expr_case_expected type_constructor
                                                operator
                                                empty
                                                name
                                                tve
                                                expected
                                                (hd :: tl)
      | None ->
         let hd_expr = a_expr tve None hd in
         let elem_type = hd_expr.ty in
         let list_type : O.type_expr =
           {type_expr = TypeApp (type_constructor, [elem_type]);
            name = None;
            orig = Region.ghost} in
         let expr_case : O.expr_case =
           App { operator = {operator;
                             args = [elem_type; list_type];
                             ret = list_type;
                             orig=`TODO};
                 arguments = [hd_expr;
                              (* Propagate the type we inferred for the first argument. *)
                              a_collection_literal_expr_case_expected type_constructor operator empty name tve list_type tl] }
    in { expr = expr_case; ty = list_type; orig = `TODO })


and a_list_expr_case tve expected arguments = a_collection_literal_expr_case List Cons  Null     "list" tve expected arguments
and a_set_expr_case  tve expected arguments = a_collection_literal_expr_case Set SetAdd EmptySet "set"  tve expected arguments

and a_cons_expr_case (tve : tve) (expected : O.type_expr option) (arguments : I.expr list) : O.expr =
  (match expected with
     Some ({type_expr = (TypeApp (List, [elem_type])); _} as list_type) ->
     (match arguments with
        [hd;tl] ->
        let expr_case : O.expr_case =
          App { operator = { operator = Cons;
                             args = [elem_type; list_type];
                             ret = list_type;
                             orig = `TODO };
                arguments = [a_expr tve (Some elem_type) hd;
                             a_expr tve (Some list_type) tl]} in
        { expr = expr_case; ty = list_type; orig = `TODO }
      | _ -> assert false
     )
   | None ->
      (match arguments with
        [hd;tl] ->
        let elem = a_expr tve None hd in
        let elem_type = elem.ty in
        let list_type : O.type_expr = { type_expr = TypeApp (List,[elem_type]);
                                        name = None;
                                        orig = Region.ghost } in
        let expr_case : O.expr_case =
          App { operator = { operator = Cons;
                             args = [elem_type; list_type];
                             ret = list_type;
                             orig = `TODO };
                arguments = [a_expr tve (Some elem_type) hd;
                             a_expr tve (Some list_type) tl]} in
        { expr = expr_case; ty = list_type; orig = `TODO }
       | _ -> assert false)
   | _ -> failwith "expected [expected] but got a list")

and a_instruction (tve : tve) : I.instr -> O.instr = function
    Assignment { name; value; orig } ->
    let te,ve = tve in
    let expected_type = lookup_v (string_of_name name) ve in
    let value = a_expr (te,ve) (Some expected_type) value
    in Assignment { name = a_name_and_region name; value; orig }
  | While { condition; body; orig } ->
     let t_bool : O.type_expr = { type_expr = Bool; name = None; orig = Region.ghost } in
     let condition = a_expr tve (Some t_bool) condition in
     let body = map (a_instruction tve) body
     in While { condition; body; orig }
  | ForCollection { list; var; body; orig } ->
     let list = a_expr tve None list in
     let elem_type = (match list.ty with
                        { type_expr = TypeApp (List, [elem_type]); _ } -> elem_type
                      | { type_expr = TypeApp (Set, [elem_type]); _ } -> elem_type
                      | { type_expr = TypeApp (Map, [k;v]); name; orig } ->
                         (* make a (k * v) pair. *)
                         (let number i x : O.name_and_region * 'todo =
                            ({ name = string_of_int i; orig = Region.ghost }, x) in
                          let t = (mapi number [k;v])
                                  |> named_list_to_map in
                          let t : O.type_expr_case = Record t in
                          { type_expr = t; name = None; orig = Region.ghost })
                      | _ -> failwith "Expected a list, set or map but got [actual = list.ty]") in
     let te,ve = tve in
     let var_type = lookup_v (string_of_name var) ve in
     check_type_expr_equal (Some var_type) elem_type;
     let var = a_name_and_region var in
     let body = map (a_instruction (te,ve)) body in
     ForCollection { list; var; body; orig }
  | Match { expr; cases; orig } ->
     let expr : O.expr = a_expr tve None expr in
     let cases = a_match_cases tve expr.ty cases in
     Match { expr; cases; orig }
  | ProcedureCall { expr; orig } ->
    let expr = a_expr tve (Some { type_expr = Unit; name = None; orig = Region.ghost }) expr
    in ProcedureCall { expr; orig }
  | Fail { expr; orig } ->
    let expr = a_expr tve (Some { type_expr = String; name = None; orig = Region.ghost }) expr
    in Fail { expr; orig }

and a_pattern (te,ve : tve) (expected : O.type_expr) (pat : I.pattern) : O.pattern * ve =
  (* TODO : for non-catch-all patterns, check that the type matches the expected one. *)
  (* TODO : check for exhaustivity and duplicate clauses *)
  match pat with
    PVar  name ->
    let ve : ve = shadow_v (string_of_name name) expected ve in
    PVar (a_name_and_region name), ve
  | PWild      ->
    PWild, ve
  | PInt    i  -> PInt    i, ve
  | PBytes  b  -> PBytes  b, ve
  | PString s  -> PString s, ve
  | PUnit      -> PUnit,     ve
  | PTrue      -> PTrue,     ve
  | PFalse     -> PFalse,    ve
  | PNone      -> PNone,     ve
  | PSome p ->
     let p,ve = a_pattern (te,ve) expected p in
     PSome p, ve
  | PCons (pl, pr) ->
     let pl,ve = a_pattern (te,ve) expected pl in
     let pr,ve = a_pattern (te,ve) expected pr in
     PCons (pl, pr), ve
  | PNull      -> PNull,     ve
  | PRecord m ->
     let m_expected =
       (match expected with
          { type_expr = Record m_expected; _ } -> m_expected
        | _ -> failwith "expected a pattern for [expected] but got a Record or Tuple pattern") in

     let expected_keys = SMap.bindings m_expected
                         |> map fst
                         |> List.sort String.compare in
     let pattern_keys = SMap.bindings m
                         |> map fst
                         |> List.sort String.compare in
     if (List.compare_lengths expected_keys pattern_keys) == 1
        && List.fold_left (fun a (x,y) -> a && (String.compare x y) == 1)
                          true
                          (List.combine expected_keys pattern_keys) then
       ()
     else
       failwith "pattern and value do not have the same keys"
     ;
     let a_field_pat s (field_name_and_region, pat) (m,ve) =
       let field_name_and_region = a_name_and_region field_name_and_region in
       let _, expected_part = SMap.find s m_expected in
       let pat,ve = a_pattern (te,ve) expected_part pat in
       let m = SMap.add s (field_name_and_region, pat) m in
       (m,ve) in
     let m, ve = SMap.fold a_field_pat m (SMap.empty, ve) in
     PRecord m, ve

and a_match_case  (te,ve : tve) (expected : O.type_expr) (pat, body : I.pattern * I.instr list) : O.pattern * O.instr list =
  let pat, ve = a_pattern (te,ve) expected pat in
  let body = map (a_instruction (te,ve)) body in
  pat, body

and a_match_cases (tve : tve) (expected : O.type_expr) (cases : (I.pattern * I.instr list) list) =
  match cases with
    [] -> failwith "No clauses in Match"
  | l -> map (a_match_case tve expected) l

and a_lambda  (te,ve : tve) (expected : O.type_expr option) ({parameter;declarations;instructions;result} : I.lambda) : O.expr =
  let {name;ty;orig} : I.typed_var = parameter in
  let ty, _todo_ignored_ctors, _ignored_fields = a_type_expr (te,ve) ty (ctors_of_tve (te,ve)) (fields_of_tve (te,ve)) in
  let parameter      : O.typed_var = {name = a_name_and_region name;ty;orig} in
  let ve = shadow_v (string_of_name name) ty ve in
  let ((te,ve),declarations) : tve * O.decl list = fold_map a_declaration (te,ve) declarations in
  let instructions = map (a_instruction (te,ve)) instructions in
  match expected with
    None ->
    let result       = a_expr (te,ve) None result; in
    let expr_case : O.expr_case = Lambda {parameter;declarations;instructions;result} in
    let function_ty : O.type_expr = {
        type_expr = Function { arg = parameter.ty; ret = result.ty };
        name = None;
        orig = Region.ghost;
      }
    in ({ expr = expr_case; ty = function_ty; orig = `TODO } : O.expr)
  | Some ({type_expr=Function { arg=expected_arg; ret=expected_ret };_} as expected) ->
     check_type_expr_equal (Some expected_arg) parameter.ty;
     let result       = a_expr (te,ve) (Some expected_ret) result; in
     let expr_case : O.expr_case = Lambda {parameter;declarations;instructions;result} in
     ({ expr = expr_case; ty = expected; orig = `TODO } : O.expr)
  | _ -> failwith "expected [expected] but got a function"

and a_xeq_expr_case (tve : tve) (expected : O.type_expr option) (arguments : I.expr list) (operator : O.operator_case) : O.expr =
  (match arguments with
     [l;r] ->
     (match expected with
        Some { type_expr = Bool; _ } -> ()
      | None -> ()
      | _ -> failwith "expected [expected] but got a boolean");
     let bool : O.type_expr = { type_expr = Bool; name = None; orig = Region.ghost } in
     let l = a_expr tve None l in
     let l_type = l.ty in
     let expr_case : O.expr_case =
       App { operator = { operator;
                          args = [l_type; l_type];
                          ret = bool;
                          orig = `TODO };
             arguments = [l;
                          a_expr tve (Some l_type) r]} in
     { expr = expr_case; ty = l_type; orig = `TODO }
   | _ -> assert false)

and a_map_lookup_expr_case (tve : tve) (expected : O.type_expr option) (arguments : I.expr list) : O.expr =
  (match arguments with
     [map_var;key] ->
     let map_var = a_expr tve None map_var in
     let tkey, tval =
       (match map_var.ty with
          { type_expr = TypeApp (Map, [tkey;tval]); _ } ->
          check_type_expr_equal expected tval;
          tkey,tval
        | _ -> failwith "Expected a map but got map_var.ty") in
     let key = a_expr tve (Some tkey) key in
     let expr_case : O.expr_case =
       App { operator = { operator = MapLookup;
                          args = [map_var.ty; tkey];
                          ret = tval;
                          orig = `TODO };
             arguments = [map_var; key]} in
     { expr = expr_case; ty = tval; orig = `TODO }
   | _ -> assert false)

and a_expr (tve : tve) (expected : O.type_expr option) : I.expr -> O.expr = function
    App      {operator=List;      arguments} -> a_list_expr_case       tve expected arguments
  | App      {operator=Set;       arguments} -> a_set_expr_case        tve expected arguments
  | App      {operator=Cons;      arguments} -> a_cons_expr_case       tve expected arguments
  | App      {operator=Equal;     arguments} -> a_xeq_expr_case        tve expected arguments Equal
  | App      {operator=Neq;       arguments} -> a_xeq_expr_case        tve expected arguments Neq
  | App      {operator=MapLookup; arguments} -> a_map_lookup_expr_case tve expected arguments
  | App      {operator;           arguments} -> a_app_expr_case        tve expected operator arguments
  | Var      var_name                        -> a_var_expr             tve expected var_name
  | Constant constant                        -> a_constant_expr        tve expected constant
  | Record   record                          -> a_record               tve expected record
  | Lambda   lambda                          -> a_lambda               tve expected lambda

and a_declaration (te,ve : tve) ({name;ty;value} : I.decl) : tve * O.decl =
  let ty, _todo_ignored_ctors, _ignored_fields = a_type_expr (te,ve) ty (ctors_of_tve (te,ve)) (fields_of_tve (te,ve)) in
  let value = a_expr (te,ve) (Some ty) value in
  let ve = shadow_v (string_of_name name) ty ve in
  let name = a_name_and_region name in
  (te,ve), {var={name;ty;orig=`TODO};value;orig = `TODO}

let a_declarations (tve : tve) (l : I.decl list) : tve * O.decl list =
  fold_map a_declaration tve l

let a_ast I.{types; storage_decl; declarations; orig} =
  let tve : tve = { types = SMap.empty; ctors = SMap.empty; fields = SMap.empty }, SMap.empty in
  let tve, types = a_types tve types in
  let tve, storage_decl = a_storage_decl tve storage_decl in
  let tve, declarations = a_declarations tve declarations in
  let _ = tve in
  O.{types; storage_decl; declarations; orig}

let annotate : I.ast -> O.ast = a_ast
