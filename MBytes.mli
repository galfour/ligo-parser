(* TEMPORARY: SHOULD BE ERASED *)

type t

val of_hex : Hex.t -> t
val to_hex : t -> Hex.t
